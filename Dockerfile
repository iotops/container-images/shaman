FROM golang:latest as builder
ARG CGO_ENABLED=0
RUN go install github.com/nanopack/shaman@latest
FROM alpine:latest
COPY --from=builder /go/bin/shaman /usr/local/bin/shaman 
CMD ["shaman","-s"]
